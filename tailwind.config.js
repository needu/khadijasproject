module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  // Setup theme
  theme: {
    screens: {
      sm: "340px",
      // => @media (min-width: 640px) { ... }

      md: "801px",
      "md-1": { max: "800px" },
      // => @media (min-width: 768px) { ... }

      lg: "1000px",
      // => @media (min-width: 1024px) { ... }

      xl: "1240px",
      "xl-1": "1320px",
      "xl-2": "1440px",
      "xl-3": "1550px",
      "1xl": "1700px",
      // => @media (min-width: 1280px) { ... }

      "2xl": "2000px",
      // => @media (min-width: 1536px) { ... }
      tall: { raw: "(max-height: 1000px)" },
      // => @media (min-height: 600px) { ... }
      // "tall-1": { "raw": "(max-height: 1000px)" },
      // // => @media (min-height: 1000px) { ... }
    },

    boxShadow: {
      sm: "0 1px 2px 0 rgba(0, 0, 0, 0.05)",
      DEFAULT:
        "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
      md: "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
      lg: "10px 10px 15px -3px rgba(0, 0, 0, 0.2), -2px -4px 20px -1px rgba(255, 255, 255, 1)",
      xl: "25px 25px 20px -3px rgba(0, 0, 0, 0.2), 1px 1px 20px -1px rgba(255, 255, 255, 1)",
      "xl-1":
        "15px 15px 20px -3px rgba(0, 0, 0, 0.1), -12px -8px 32px -1px rgba(255, 255, 255, 1)",
      "2xl": "0 25px 50px -12px rgba(0, 0, 0, 0.25)",
      "3xl": "0 35px 60px -15px rgba(0, 0, 0, 0.3)",
      "clientHeader":"0px 0px 0px 1px rgba(38, 47, 54, 0.08), 0px 2px 3px rgba(38, 47, 54, 0.1)",
      "clientCard":"0px 0px 0px 1px rgba(38, 47, 54, 0.08), 0px 2px 3px rgba(38, 47, 54, 0.1)",
      "calendlyProfileCard" : "0px 0px 0px 1px rgba(38, 47, 54, 0.08)",
      "clientHeaderMenu" : "0px 5px 15px 0px rgba(0, 0, 0, 0.2)",
      inner: "inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)",
      none: "none",
      bottom: "0px 5px 10px -3px rgba(0, 0, 0, 0.3)",
    },

    // Colors
    colors: {
      // White
      white: {
        1: "#fff",
        2: "#fbfafa",
        3: "#d9d9d9",
        4: "#bfbfbf",
        5: "#E8EFF4",
        6: "#F1F2F5",
      },

      // Grey
      grey: {
        1: "#C4C4C4",
        2: "#EFEFEF",
        3: "#E5E5E5",
        4: "#D3D3D3",
        5: "#A5ACB8",
      },

      // Black
      black: {
        1: "#000",
        2: "#6B6C7E",
        3: "#0B0C0C",
      },

      // Orange
      orange: {
        1: "orange",
      },

      // Green
      green: {
        1: "green",
        2: "#DEF0FF",
      },

      // Red
      red: {
        1: "red",
      },

      //blue
      blue: {
        1: "#0B5FFF",
        2: "#0A1656",
        3: "#0E23D0",
        4: "#204ECF",
        5: "#FFFFFF",
        6: "#C4DBF6",
        7: "#E1EEF9",
        8: "#0F69F6",
        9: '#51ABFF',
        hover: "#4C8BFF",
      },

      //pink
      pink: {
        1: "#FF4974",
        2: "#ea9aa8",
        hover: "#F47990",
      },

      // Accent
      accent: {
        DEFAULT: "#0c3d6d",
        fill: "#ccecfd",
        primary: "#1890ff",
        hover: "#40a9ff",
      },

      // Alert
      alert: {
        success: {
          DEFAULT: "#f6ffed",
          fill: "#5cad1f",
          border: "#b7eb8f",
        },
        info: {
          DEFAULT: "#e6f7ff",
          fill: "#007ecc",
          border: "#91d5ff",
        },
        warning: {
          DEFAULT: "#fffbe6",
          fill: "#cc9c00",
          border: "#ffe58f",
        },
        error: {
          DEFAULT: "#fff2f0",
          fill: "#cc1100",
          border: "#ffccc7",
          hover: "#b20000",
        },
      },
    },

    extend: {
      height: {
        "1xl": "250px",
        "2xl": "150px",
      },
      inset: {
        "16-1": "4.88rem",
      },
      lineHeight: {
        11: "3.25rem",
        12: "57px",
      },
      padding: {
        13: "3.25rem",
      },
      margin: {
        "-29": "-7.5rem",
        26: "6.625rem",
      },
      fontSize: {
        "7xl-1": "5.439rem",
      },
    },
  },
  plugins: [],
};
