import React, { useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";


import { Grid, Paper, TextField, Button } from "@mui/material";

const Signup = () => {
  //Initializing states
  const [name, setName]  = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const navigate = useNavigate();

  const paperStyle = {
    padding: "20px",
    height: "70vh",
    width: 380,
    margin: "20px auto",
    borderRadius: "20px",
    shadowColor: "blue",
  };
  const btnstyle = { margin: "20px 0", height: "3.5em" };
  const imgStyle = { margin: "10px auto", width: "8em", height: "1.5em" };

  useEffect(() => {
    document.title = "Signup";
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    navigate('/Dashboard')
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="center"
      style={{ height: "100vh" }}
    >
      <Paper elevation={10} style={paperStyle}>
        <Grid
          container
          direction="column"
          justifyContent="space-between"
          alignItems="center"
          style={{ marginTop: "40px" }}
        >
          <Grid align="center">
            <h2>Sign In</h2>
          </Grid>
          <form onSubmit={handleSubmit}>
          <TextField
              label="Name"
              placeholder="Enter Name"
              variant="outlined"
              margin="normal"
              fullWidth
              required
              value={name}
              onClick={() => setErrorMessage("")}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
            <TextField
              label="Username"
              placeholder="Enter username"
              variant="outlined"
              margin="normal"
              fullWidth
              required
              value={email}
              onClick={() => setErrorMessage("")}
              onChange={(e) => {
                setEmail(e.target.value);
              }}
            />
            <TextField
              label="Password"
              placeholder="Enter password"
              type="password"
              variant="outlined"
              margin="normal"
              fullWidth
              required
              value={password}
              onClick={() => setErrorMessage("")}
              onChange={(e) => setPassword(e.target.value)}
            />
            {errorMessage && (
              <p style={{ color: "red", margin: "10px 0" }}>{errorMessage}</p>
            )}
            <Button
              type="submit"
              color="primary"
              variant="contained"
              size="large"
              style={btnstyle}
              fullWidth
            >
              Sign in
            </Button>
          </form>
        </Grid>
      </Paper>
    </Grid>
  );
};

export default Signup;