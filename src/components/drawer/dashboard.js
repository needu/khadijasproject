import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';

import UsersDataGrid from '../usersDataGrid/usersDataGrid'

const drawerWidth = 240;

export default function PermanentDrawerLeft() {

    const [selectedIndex, setSelectedIndex] = React.useState(0);

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  };


  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="permanent"
        anchor="left"
      >
        <Toolbar />
        <Divider />
        <List>
            <ListItemButton
            selected={selectedIndex === 0}
            onClick={(event) => handleListItemClick(event, 0)}
            >
              <ListItemText primary='Home' />
            </ListItemButton>
            <ListItemButton
            selected={selectedIndex === 1}
            onClick={(event) => handleListItemClick(event, 1)}
            >
              <ListItemText primary='Users' />
            </ListItemButton>
            <ListItemButton
            selected={selectedIndex === 2}
            onClick={(event) => handleListItemClick(event, 2)}
            >
              <ListItemText primary='Mail' />
            </ListItemButton>
            <ListItemButton
            selected={selectedIndex === 3}
            onClick={(event) => handleListItemClick(event, 3)}
            >
              <ListItemText primary='Reports' />
            </ListItemButton>
        </List>
      </Drawer>
      <Box
        component="main"
        sx={{ flexGrow: 1, bgcolor: 'background.default', p: 3 }}
      >
        {
          selectedIndex === 0 ? <Typography paragraph>
            Here You Go ....
          </Typography>
          : selectedIndex === 1 ? <UsersDataGrid />
          : selectedIndex === 2 ? <Typography paragraph>
            Mails ....
          </Typography>
          : <Typography paragraph>
          Reports ....
        </Typography>
        }
      </Box>
    </Box>
  );
}
