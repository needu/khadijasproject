import * as React from 'react';
import { DataGrid, GridActionsCellItem } from '@mui/x-data-grid';
import DeleteIcon from '@mui/icons-material/Delete';

// const columns = [
//   { field: 'id', headerName: 'ID', width: 100 },
//   { field: 'user', headerName: 'User', width: 200 },
//   {
//     field: 'email',
//     headerName: 'Email',
//     width: 250,
//   },
//   {
//     field: 'status',
//     headerName: 'Status',
//     // description: 'This column has a value getter and is not sortable.',
//     // sortable: false,
//     width: 100,
//     // valueGetter: (params) =>
//     //   `${params.row.firstName || ''} ${params.row.lastName || ''}`,
//   },
//   {
//     field: 'action',
//     headerName: 'Actions',
//     // description: 'This column has a value getter and is not sortable.',
//     // sortable: false,
//     width: 200,
//     // valueGetter: (params) =>
//     //   `${params.row.firstName || ''} ${params.row.lastName || ''}`,
//   },
// ];

const initialRows  = [
  { id: 1, user: 'Snow', email: 'Jon', status: 'active' },
  { id: 2, user: 'Lannister', email: 'Cersei', status: 'active' },
  { id: 3, user: 'Lannister', email: 'Jaime', status: 'active' },
  { id: 4, user: 'Stark', email: 'Arya', status: 'active' },
  { id: 5, user: 'Targaryen', email: 'Daenerys', status: 'active' },
  { id: 6, user: 'Melisandre', email: null, status: 'active' },
  { id: 7, user: 'Clifford', email: 'Ferrara', status: 'active' },
  { id: 8, user: 'Frances', email: 'Rossini', status: 'active' },
  { id: 9, user: 'Roxie', email: 'Harvey', status: 'active' },
];

export default function DataTable() {

    const [rows, setRows] = React.useState(initialRows);

    const deleteUser = React.useCallback(
        (id) => () => {
          setTimeout(() => {
            setRows((prevRows) => prevRows.filter((row) => row.id !== id));
          });
        },
        [],
      );

      const columns = React.useMemo(
        () => [
            { field: 'id', headerName: 'ID', width: 100 },
            { field: 'user', headerName: 'User', width: 200 },
            {
                field: 'email',
                headerName: 'Email',
                width: 250,
            },
            {
                field: 'status',
                headerName: 'Status',
                // description: 'This column has a value getter and is not sortable.',
                // sortable: false,
                width: 100,
                // valueGetter: (params) =>
                //   `${params.row.firstName || ''} ${params.row.lastName || ''}`,
            },
            {
                field: 'actions',
                type: 'actions',
                width: 80,
                getActions: (params) => [
                <GridActionsCellItem
                    icon={<DeleteIcon />}
                    label="Delete"
                    onClick={deleteUser(params.id)}
                />,
                // <GridActionsCellItem
                //     icon={<SecurityIcon />}
                //     label="Toggle Admin"
                //     onClick={toggleAdmin(params.id)}
                //     showInMenu
                // />,
                // <GridActionsCellItem
                //     icon={<FileCopyIcon />}
                //     label="Duplicate User"
                //     onClick={duplicateUser(params.id)}
                //     showInMenu
                // />,
                ],
            },
        ],
        [deleteUser],
      );    


  return (
    <div style={{ height: 550, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={8}
        rowsPerPageOptions={[8]}
        checkboxSelection
      />
    </div>
  );
}
